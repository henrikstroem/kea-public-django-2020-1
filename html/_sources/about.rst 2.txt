About the Course
================

.. csv-table:: Django Course
   :widths: 40, 40

   "Title", "Django"
   "Study Programme", "Web Development"
   "Type of Education", "Full Time Education"
   "Level of Education", "Bachelor (top-up)"
   "Semester", "5 or 6"
   "Duration of the Module", "1 semester or 16 weeks"
   "ECTS", "10"
   "Expected Workload", "280 hours"
   "Programme Elemements", "Elective"
   "Language", "English"
   "Start Time", "Spring and Autum"
   "Location", "Lygten 37, København NV"
   "Responsible for the Module", "Henrik Strøm"

Objectives
----------

The objective of the course is to qualify the student to develop advanced web applications in the fully-featured web development framework Django, using best practice methods, and apply various web-based protocols and technologies correctly and effectively.


Knowledge
---------

The objective is to give the student knowledge of:

* Python programming
* Django framework features and design approach
* Model-View-Template design pattern
* standard web-based protocols and technologies
* best practice web-development methods
* issues related to real-world web hosting

Skills
------

The objective is that the student will have acquired the ability to:

* design and implement advanced web-based applications and APIs using the Python programming language and the Django framework
* use tests to ensure code quality
* implement a REST API
* defer long-running tasks outside the request/response cycle
* use middleware to interact with requests and responses
* use signals to trigger event-based actions
* use channels to facilitate asynchronous communication

Proficiencies
-------------

The objective is that the student will have acquired proficiency to design and develop advanced web-based applications using the Django framework.

Type of Instruction
-------------------

The teaching is organized as a variation between class teaching, guest lecturing, company visits, group project work and individual work. 
The learning is most often problem-based and cross-disciplinary and always practice-oriented. 
In addition to learning the subject, the student will gain the competences to work individually and in collaboration with others.
The common aim of the activities is always to set clear intended learning objectives.

Mandatory Hand-Ins
------------------

You must turn in the two mandatory assignments and have them approved to attend the exam for this course.

The second mandatory assignment is the foundation for your exam project - this means that you will have feedback on your work, and will be able to make improvements.

Exam
----

30 minutes oral exam based on project hand in:

* 10 minutes for your presentation
* 15 minutes for questions
* 5 minutes for grading
