Week 06 - Course Intro
======================

Who am I
--------
Henrik Strøm (`hstr@kea.dk <mailto:hstr@kea.dk>`_)

* Lecturer at KEA
* Ph.D. Fellow at Aalborg University
* M.Sc. in Engineering (cand. polyt.)
* Master in Information and Communication Technologies (mICT)
* Specialize in machine learning, artificial intelligence, and data science
* Worked in Denmark, China, Thailand; also projects in USA, Singapore
* Worked in major enterprise industry, financial sector, start-up
* Been at KEA since May 1st, 2018

.. figure:: _static/vcard-public.png
    :width: 200px
    :align: center
    :height: 200px
    :alt: Henrik Strøm's VCARD
    :figclass: align-center

    My contact info. My LinkedIn info is there, you are welcome to connect.

Quick Poll
----------

How many of you ...

* Know Python?
* Know Django?

Please present yourself:

* Name
* Expectations for this course

Software Needed for this Course
-------------------------------

You need the following software - make sure you have it installed and that it is working:

* Git - you can use the terminal or a GUI
* Python 3.7 or newer
* The course materials: `example.com <example.com>`_

Python Environments
-------------------

.. note:: If you are running Windows please install `Linux Subsystem for Windows <https://docs.microsoft.com/en-us/windows/wsl/install-win10>`_. My instructions and ability to help will only cover UNIX-like systems.

In this course we will be using Python 3.7 or newer.

.. code-block:: bash

   $ python3 -V
   Python 3.8.1

To separate Python installations and installed packages for different projects we usually create separate environments:

.. code-block:: bash

   $ mkdir ~/py-env
   $ python3 -m venv py-env/django-env

Once you have created an environment you can activate it:

.. code-block:: bash

   $ source py-env/django-env/bin/activate
   (django-env) $

The environment name in parenthesis indicate that the environment is activated.

If you put all your environments in one folder it is easy to configure most editors and IDEs too look for environments in that folder, e.g. `~/py-env/`

You can then install packages into your environment without changing your computer's system level installation of Python, e.g. to install Django:

.. code-block:: bash

   (django-env) $ pip install django

Quick-and-Dirty Intro to Python
-------------------------------

.. note:: Live Demo
